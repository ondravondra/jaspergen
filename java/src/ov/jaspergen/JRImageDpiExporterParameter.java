package ov.jaspergen;

import net.sf.jasperreports.engine.JRExporterParameter;

public class JRImageDpiExporterParameter extends JRExporterParameter {
    protected JRImageDpiExporterParameter() {
        super("net.sf.jasperreports.image.dpi");
    }
}
