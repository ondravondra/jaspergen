package ov.jaspergen;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface IJasperGenReport {
    void setParam(String key, Object value);
    void setDataSourceMapCollection(List<Map<String, ?>> collection);
    void setDataSourceMSSQL(String serverHostName, String databaseName, String userName, String password) throws ClassNotFoundException, SQLException;
}
