package ov.jaspergen;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.bouncycastle.util.encoders.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JasperGenReport implements IJasperGenReport {
    private String compiledJasperFile;
    private Map<String, Object> parameters;
    private Connection dataConnection;
    private JRDataSource dataSource;

    private OutputFormat outputFormat;
    private String outputFile;

    public JasperGenReport(String compiledJasperFile, OutputFormat outputFormat, String outputFile) {
        this.compiledJasperFile = compiledJasperFile;
        this.outputFormat = outputFormat;
        this.outputFile = outputFile;
        parameters = new HashMap<String, Object>();
    }

    @Override
    public void setParam(String key, Object value) {
        parameters.put(key, value);
    }

    private void resetDataSources() {
        dataConnection = null;
        dataSource = null;
    }

    @Override
    public void setDataSourceMSSQL(String serverHostName, String databaseName, String userName, String password) throws ClassNotFoundException, SQLException {
        resetDataSources();
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String url = "jdbc:sqlserver://" + serverHostName + ":1433;databaseName=" + databaseName;
        dataConnection = DriverManager.getConnection(url, userName, password);
    }

    @Override
    public void setDataSourceMapCollection(List<Map<String, ?>> collection) {
        resetDataSources();
        dataSource = new JRMapCollectionDataSource(collection);
    }

    public void print() throws Exception {
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File(compiledJasperFile));
        JasperPrint jasperPrint;
        if (dataConnection != null) {
            jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, dataConnection);
        } else {
            jasperPrint = JasperFillManager.fillReport(
                    jasperReport, parameters, dataSource != null ? dataSource : new JREmptyDataSource());
        }

        switch (outputFormat) {
            case PDF:
                JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
                break;
            case HTML:
                JRHtmlExporter htmlExporter = new JRHtmlExporter();
                htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                htmlExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputFile);
                htmlExporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
                htmlExporter.setParameter(JRHtmlExporterParameter.ZOOM_RATIO, 2.0f);
                htmlExporter.setParameter(new JRImageDpiExporterParameter(), 150.0f);
                htmlExporter.exportReport();
                inlineImages(outputFile);
                break;
        }
    }

    private void inlineImages(String htmlFileName) throws IOException {
        File htmlFile = new File(htmlFileName);

        FileInputStream instr = new FileInputStream(htmlFile);
        byte [] buffer = new byte[(int)htmlFile.length()];
        int read = instr.read(buffer);
        instr.close();
        String htmlText = new String(buffer, 0, read, "UTF-8");

        StringBuilder resultText = new StringBuilder();
        int idx = 0;
        Pattern pattern = Pattern.compile("(<img[^>]+src\\s*=\\s*\")([^\"]+)(\"[^>]*>)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(htmlText);
        while (matcher.find()) {
            resultText.append(htmlText.substring(idx, matcher.start()));
            resultText.append(matcher.group(1));

            String src = matcher.group(2);
            String imgFileName = htmlFile.getParent() + "\\" + src.replace('/', '\\');
            byte [] imgData = readFileData(imgFileName);

            StringBuilder newData = new StringBuilder();
            newData.append("data:image/png;base64,");
            newData.append(new String(Base64.encode(imgData)));
            resultText.append(newData.toString());

            resultText.append(matcher.group(3));

            idx = matcher.end();
        }
        resultText.append(htmlText.substring(idx));

        FileOutputStream outstr = new FileOutputStream(htmlFile);
        outstr.getChannel().truncate(0);
        outstr.write(resultText.toString().getBytes("UTF-8"));
        outstr.close();
    }

    private byte [] readFileData(String fileName) throws IOException {
        File f = new File(fileName);
        FileInputStream fis = new FileInputStream(f);
        byte [] data = new byte[(int)f.length()];
        //noinspection ResultOfMethodCallIgnored
        fis.read(data);
        return data;
    }
}
