package ov.jaspergen;

public enum OutputFormat {
    HTML,
    PDF
}
