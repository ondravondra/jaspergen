package ov.jaspergen;

import groovy.lang.Binding;
import groovy.util.GroovyScriptEngine;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

import java.io.File;

public class JasperGen {
    public static void main(String[] args) throws Exception {
        if (args.length > 1 && args[0].equals("--compile")) {
            compileReports(args[1]);
            System.exit(0);
            return;
        }

        if (args.length > 3 && !args[0].startsWith("--")) {
            runReport(args[0], args[1], OutputFormat.valueOf(args[2].toUpperCase()), args[3]);
            System.exit(0);
            return;
        }

        System.err.println("Report compilation: --compile <jrxml-file-or-folder>");
        System.err.println("Report generation: <jasper-compiled-file> <input-groovy-file> html|pdf <output-file>");
        System.exit(1);
    }

    private static void runReport(String compiledJasperFile, String inputGroovyFile, OutputFormat outputFormat, String outputFile) throws Exception {
        File groovyFile = new File(inputGroovyFile);
        String[] roots = new String[]{groovyFile.getParentFile().getAbsolutePath()};
        GroovyScriptEngine gse = new GroovyScriptEngine(roots);
        Binding binding = new Binding();

        JasperGenReport jg = new JasperGenReport(compiledJasperFile, outputFormat, outputFile);
        binding.setVariable("jasperGen", jg);

        gse.run(groovyFile.getName(), binding);

        jg.print();
    }

    private static void compileReports(String fileOrFolder) throws Exception {
        File ff = new File(fileOrFolder);
        if (ff.isFile()) {
            compileReport(ff);
            return;
        }
        if (!ff.isDirectory()) {
            return;
        }

        String fileName;
        File[] listOfFiles = ff.listFiles();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    fileName = file.getName();
                    if (fileName.toLowerCase().endsWith(".jrxml")) {
                        compileReport(file);
                    }
                }
            }
        }
    }

    private static void compileReport(File file) throws JRException {
        String fn = file.getName();
        if (fn.endsWith(".jrxml")) {
            fn = fn.substring(0, fn.length() - 6);
        }
        JasperCompileManager.compileReportToFile(file.getAbsolutePath(), file.getParentFile().getAbsolutePath() + "\\" + fn + ".jasper");
    }
}
