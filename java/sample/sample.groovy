jasperGen.setParam("testParam1", "Ahoj, světe!")
jasperGen.setParam("testParam2", new Date())

ArrayList dataSet = []
dataSet.add(["Person":"Adam","Beers":3])
dataSet.add(["Person":"Bedřich","Beers":5])
dataSet.add(["Person":"Cecílie","Beers":2])
dataSet.add(["Person":"Daniela","Beers":1])

jasperGen.setDataSourceMapCollection(dataSet)