﻿using System;

namespace jaspergen
{
    public class JasperGenException : Exception
    {
        public int ErrorCode { get; private set; }
        public string ErrorOutput { get; private set; }

        public JasperGenException(int errorCode, string errorOutput)
            : base("JasperGen returned with error code " + errorCode + ".")
        {
            ErrorCode = errorCode;
            ErrorOutput = errorOutput;
        }
    }
}
