﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace jaspergen
{
    public static class JasperGen
    {
        private static string installationDirectory;
        private static string overrideJavaHome;

        public static void SetInstallationDir(string installationDir)
        {
            installationDirectory = installationDir;
        }

        public static void SetJavaHome(string javaHome)
        {
            overrideJavaHome = javaHome;
        }

        private static string FindJavaHome()
        {
            if (!string.IsNullOrEmpty(overrideJavaHome))
            {
                return overrideJavaHome;
            }
            return Environment.GetEnvironmentVariable("JAVA_HOME");
        }

        private static void CheckSettings()
        {
            if (string.IsNullOrEmpty(installationDirectory))
            {
                throw new Exception("Installation directory not specified.");
            }
            if (string.IsNullOrEmpty(FindJavaHome()))
            {
                throw new Exception("JAVA_HOME not specified.");
            }
        }

        public static bool CanRun
        {
            get
            {
                try
                {
                    CheckSettings();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static Report Report(string jasperFile)
        {
            return new Report(jasperFile);
        }

        public static int CompileReportDir(string directory)
        {
            return Run("--compile \"" + directory + "\"");
        }

        internal static int Run(string arguments, StringBuilder stdErr = null)
        {
            CheckSettings();

            var path = Path.Combine(FindJavaHome(), "bin\\java.exe");
            var args = new StringBuilder();
            args.Append("-Dfile.encoding=UTF-8 ");
            args.Append("-classpath \"" + Path.Combine(installationDirectory, "jaspergen.jar") + "\" ");
            args.Append("ov.jaspergen.JasperGen");
            if (!string.IsNullOrEmpty(arguments))
            {
                args.Append(" ");
                args.Append(arguments);
            }

            var process = new Process
            {
                StartInfo = new ProcessStartInfo(path)
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    RedirectStandardError = stdErr != null
                }
            };
            process.StartInfo.Arguments = args.ToString();
            process.Start();
            process.WaitForExit();
            if (stdErr != null)
            {
                stdErr.Append(process.StandardError.ReadToEnd());
            }
            return process.ExitCode;
        }

        internal static int RunReport(string jasperFile, IEnumerable<string> groovyLines, string outputFormatType, Stream output, StringBuilder stdErr = null)
        {
            var groovyFile = Path.GetTempFileName() + ".groovy";
            var outFile = Path.GetTempFileName();

            using (var fstr = File.CreateText(groovyFile))
            {
                foreach (var l in groovyLines)
                {
                    fstr.WriteLine(l);
                }
            }

            var exitCode = Run("\"" + jasperFile + "\" \"" + groovyFile + "\" " + outputFormatType + " \"" + outFile + "\"", stdErr);
            File.Delete(groovyFile);
            if (exitCode != 0)
            {
                return exitCode;
            }

            using (var fstr = File.OpenRead(outFile))
            {
                var buf = new byte[32768];
                int read;
                while ((read = fstr.Read(buf, 0, buf.Length)) > 0)
                {
                    output.Write(buf, 0, read);
                }
            }

            File.Delete(outFile);
            return exitCode;
        }
    }
}
