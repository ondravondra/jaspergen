﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using jaspergen.helper;

namespace jaspergen
{
    public class Report
    {
        private readonly string jasperFile;
        private readonly List<string> groovyLines = new List<string>();

        internal Report(string jasperFile)
        {
            this.jasperFile = jasperFile;
        }

        public void AddStatement(string text)
        {
            groovyLines.Add(text);
        }

        public void SetStringParameter(string name, string value)
        {
            SetExpressionParameter(name, GroovyHelper.GetStringTerm(value));
        }

        public void SetIntParameter(string name, int value)
        {
            SetExpressionParameter(name, GroovyHelper.GetIntTerm(value));
        }

        public void SetDoubleParameter(string name, double value)
        {
            SetExpressionParameter(name, GroovyHelper.GetDoubleTerm(value));
        }

        public void SetDateTimeParameter(string name, DateTime value)
        {
            SetExpressionParameter(name, GroovyHelper.GetDateTerm(value));
        }

        public void SetSqlTimestampParameter(string name, DateTime value)
        {
            SetExpressionParameter(name, GroovyHelper.GetSqlTimestampTerm(value));
        }

        public void SetParameters(object paramObj)
        {
            foreach (var kv in GroovyHelper.ParseParamObject(paramObj))
            {
                SetExpressionParameter(kv.Key, kv.Value);
            }
        }

        public void SetExpressionParameter(string name, string expression)
        {
            groovyLines.Add("jasperGen.setParam(\"" + name + "\", " + expression + ")");
        }

        public void SetMSSQLDataSource(string hostName, string databaseName, string user, string password)
        {
            groovyLines.Add(
                "jasperGen.setDataSourceMSSQL(" +
                GroovyHelper.GetStringTerm(hostName) +
                ", " + GroovyHelper.GetStringTerm(databaseName) +
                ", " + GroovyHelper.GetStringTerm(user) +
                ", " + GroovyHelper.GetStringTerm(password) +
                ")");
        }

        public void SetMapCollectionDataSource(IEnumerable mapCollectionValues)
        {
            groovyLines.Add("ArrayList dataSet = []");
            foreach (var v in mapCollectionValues)
            {
                groovyLines.Add("dataSet.add(" + GroovyHelper.GetMap(v) + ")");
            }
            groovyLines.Add("jasperGen.setDataSourceMapCollection(dataSet)");
        }

        public int TryPrint(OutputFormat outputFormat, Stream output)
        {
            return JasperGen.RunReport(jasperFile, groovyLines, outputFormat.ToString(), output);
        }

        public void Print(OutputFormat outputFormat, Stream output)
        {
            var err = new StringBuilder();
            var code = JasperGen.RunReport(jasperFile, groovyLines, outputFormat.ToString(), output, err);
            if (code != 0)
            {
                throw new JasperGenException(code, err.ToString());
            }
        }
    }
}
