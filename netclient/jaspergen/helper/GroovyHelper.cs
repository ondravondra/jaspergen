﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace jaspergen.helper
{
    public static class GroovyHelper
    {
        public static string EscapeString(string value)
        {
            return value.Replace(@"\", @"\\").Replace("\"", "\\\"");
        }

        public static string GetStringTerm(string value)
        {
            return "\"" + EscapeString(value) + "\"";
        }

        public static string GetIntTerm(int value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public static string GetDoubleTerm(double value)
        {
            return value.ToString("R").Replace(',', '.') + "D";
        }

        public static string GetDateTerm(DateTime value)
        {
            return "new Date(" +
                   (value.Year - 1900) + "," + (value.Month - 1) + "," + value.Day + "," +
                   value.Hour + "," + value.Minute + "," + value.Second + ")";
        }

        public static string GetSqlTimestampTerm(DateTime value)
        {
            return "new java.sql.Timestamp(" +
                   (value.Year - 1900) + "," + (value.Month - 1) + "," + value.Day + "," +
                   value.Hour + "," + value.Minute + "," + value.Second +
                   "," + (value.Millisecond * 1000000) + ")";
        }
        
        private delegate string ToTermMethod(object value);

        private static readonly Dictionary<Type, ToTermMethod> toTermMethods =
            new Dictionary<Type, ToTermMethod>
            {
                { typeof(string), value => GetStringTerm((string)value) },
                { typeof(int), value => GetIntTerm((int)value) },
                { typeof(double), value => GetDoubleTerm((double)value) },
                { typeof(DateTime), value => GetDateTerm((DateTime)value) },
            };

        private static bool CallToTermMethod(Type type, object value, out string term)
        {
            if (!toTermMethods.ContainsKey(type))
            {
                term = null;
                return false;
            }
            term = toTermMethods[type](value);
            return true;
        }

        public static IEnumerable<KeyValuePair<string, string>> ParseParamObject(object paramObj)
        {
            string v;
            foreach (var m in paramObj.GetType().GetMembers())
            {
                var pi = m as PropertyInfo;
                if (pi != null && CallToTermMethod(pi.PropertyType, pi.GetValue(paramObj, null), out v))
                {
                    yield return new KeyValuePair<string, string>(m.Name, v);
                }
                var fi = m as FieldInfo;
                if (fi != null && CallToTermMethod(fi.FieldType, fi.GetValue(paramObj), out v))
                {
                    yield return new KeyValuePair<string, string>(m.Name, v);
                }
            }
        }

        public static string GetMap(object values)
        {
            var result = new StringBuilder();
            result.Append("[");
            var first = true;
            foreach (var kv in ParseParamObject(values))
            {
                if (!first)
                {
                    result.Append(",");
                }
                else
                {
                    first = false;
                }

                result.Append("\"");
                result.Append(kv.Key);
                result.Append("\":");
                result.Append(kv.Value);
            }
            result.Append("]");
            return result.ToString();
        }
    }
}
