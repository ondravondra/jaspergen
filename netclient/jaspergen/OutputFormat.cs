﻿using System;

namespace jaspergen
{
    public enum OutputFormat
    {
        HTML, PDF
    }
}
