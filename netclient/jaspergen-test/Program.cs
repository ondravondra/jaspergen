﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using jaspergen;

namespace jaspergen_test
{
    class Program
    {
        static void Main()
        {
            var baseDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase.Replace("file:///", ""));

            Debug.Assert(baseDir != null, "baseDir != null");
            var javaDir = Path.Combine(baseDir, @"..\..\..\..\java");

            Debug.Assert(javaDir != null, "javaDir != null");
            var sampleDir = Path.Combine(javaDir, "sample");
            var artifactsDir = Path.Combine(javaDir, @"out\artifacts\jaspergen");

            JasperGen.SetInstallationDir(artifactsDir);

            Debug.Assert(sampleDir != null, "sampleDir != null");

            JasperGen.CompileReportDir(sampleDir);
            
            var rpt = JasperGen.Report(Path.Combine(sampleDir, "sample.jasper"));

            rpt.SetParameters(
                new
                {
                    testParam1 = "Ahoj, \"světe\"! Lomítko: \\",
                    testParam2 = DateTime.Now
                });

            var data = new List<object>
                       {
                           new { Person = "Adam", Beers = 3 },
                           new { Person = "Bedřich", Beers = 5 },
                           new { Person = "Cecílie", Beers = 2 },
                           new { Person = "Daniela", Beers = 1 }
                       };
            rpt.SetMapCollectionDataSource(data);

            Console.WriteLine("Printing PDF ...");

            using (var strPdf = File.Create("sample.pdf"))
            {
                rpt.Print(OutputFormat.PDF, strPdf);
            }

            Console.WriteLine("Printing HTML ...");

            using (var strHtml = File.Create("sample.html"))
            {
                rpt.Print(OutputFormat.HTML, strHtml);
            }
        }
    }
}
